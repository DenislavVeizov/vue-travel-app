import { createRouter, createWebHashHistory } from "vue-router";
import Home from "@/views/Home";
import Brazil from "@/views/Brazil";
import Hawaii from "@/views/Hawaii";
import Jamaica from "@/views/Jamaica";
import Panama from "@/views/Panama";

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/brazil",
    name: "brazil",
    component: Brazil
  },
  {
    path: "/hawaii",
    name: "hawaii",
    component: Hawaii
  },
  {
    path: "/jamaica",
    name: "jamaica",
    component: Jamaica
  },
  {
    path: "/panama",
    name: "panama",
    component: Panama
  }
];

const router = createRouter({
  history: createWebHashHistory(),
  routes
});

export default router;
